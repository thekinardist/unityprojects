﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 
public class BackToStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine ("back"); 
	}
	
	// Update is called once per frame
	void Update () {
	}
	IEnumerator back(){
		yield return new WaitForSeconds (2.0f); 
		SceneManager.LoadScene("TitleScreen"); 

	}
}
