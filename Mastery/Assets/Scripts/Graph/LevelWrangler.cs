﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class LevelWrangler : MonoBehaviour
{
    public Transform cam, graph, gamePrefabs;
    //---KEYS FOR DIFFERENT INTERACTIONS WITHIN ENVIRONMENT---// 

    private GameObject[] planes;
    public PathType pathType = PathType.CatmullRom;
    public Vector3[] campoints = new[] {
    new Vector3(0,2,0),
    new Vector3(1,2,0),
    new Vector3(1,0,0),
    new Vector3(-1,0,0)
    };

    // Use this for initialization

    void Awake()
    {
        planes = GameObject.FindGameObjectsWithTag("plane");
    }
    void Start()
    {

        DOTween.SetTweensCapacity(4000, 500);
        foreach (GameObject plane in planes)
        {
            plane.transform.DORotate(new Vector3(plane.transform.rotation.x, plane.transform.rotation.y, plane.transform.rotation.z + 90), 3, RotateMode.Fast).SetLoops(-1, LoopType.Incremental).SetEase(Ease.InOutQuad);
        }
        Tween a = cam.DOLocalPath(campoints, 15, pathType)
        .SetOptions(false).SetLookAt(new Vector3(graph.position.x, graph.position.y, graph.position.z));
        a.SetEase(Ease.InOutQuad).SetLoops(-1);

    }

    // Update is called once per frame
    void Update()
    {
        //graph.DORotate(new Vector3(0,graph.rotation.y + 60,0),6).SetLoops(-1,LoopType.Yoyo); 
        graph.Rotate(0, 150 * Time.deltaTime, 0);
        foreach (GameObject plane in planes)
        {
            plane.transform.Rotate(0, 0, 70 * Time.deltaTime);
        }
    }
}
