﻿using UnityEngine; 

public enum GraphName{
	Sine,
	Sine2D,
	MultiSine, 
	MultiSine2D, 
	Ripple, 
	Cylinder, 
	TwistingCylinder,
	Sphere, 
	WobbleSphere, 
	SpindleTorus,
	RingTorus
}