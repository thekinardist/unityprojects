
#if !defined(MY_LIGHTING_INCLUDED)
#define MY_LIGHTING_INCLUDED

#include "AutoLight.cginc"
#include "UnityPBSLighting.cginc"

sampler2D _NormalMap; 
// float4 _HeightMap_TexelSize; 
//color
float4 _Tint;
//texture for material
float4 _MainTex_ST;
float4 _MainTex; 
//metallic for reflectivity
float4 _Metallic;
float4 _HeightMap_TexelSize; 
//smoothness
float _Smoothness;

struct VertexData
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD0;
};

struct Interpolators
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD0;
	float3 normal : TEXCOORD1;
	float3 worldPos : TEXCOORD2;

#if defined(VERTEXLIGHT_ON)
	float3 vertexLightColor : TEXCOORD3;
#endif
};

void ComputeVertexLightColor(inout Interpolators i)
{
#if defined(VERTEXLIGHT_ON)
	// float3 lightVec = lightPos - i.WorldPos;
	// float3 lightDir = normalize(lightVec);
	// float ndotl = DotClamped(i.normal, lightdir);
	// float attenuation = 1 / (1 + dot(lightDir, lightDir) * unity_4LightAtten0.x); 
	// i.vertexLightColor = unity_LightColor[0].rgb * ndotl * attenuation;
	i.vertexLightColor = Shade4PointLights(
		unity4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0, 
		unity_LightColor[0].rgb, 
		unityLightColor[1].rgb, 
		unity_LightColor[2].rgb, 
		unity_LightColor[3].rgb,
		 unity_4LightAtten0, i.worldPos, i.normal); 
#endif
}

Interpolators MyVertexProgram(VertexData v)
{
	Interpolators i;
	i.position = UnityObjectToClipPos(v.position);
	i.worldPos = mul(unity_ObjectToWorld, v.position);
	i.normal = UnityObjectToWorldNormal(v.normal);
	i.uv = TRANSFORM_TEX(v.uv, _MainTex);
	ComputeVertexLightColor(i);
	return i;
}

//---LIGHT DIFFUSION---///
UnityLight CreateLight(Interpolators i)
{
	UnityLight light;
#if defined(POINT) || defined(POINT_COOKIE) || defined(SPOT)
	light.dir = normalize(_WorldSpaceLightPos0.xyz - i.worldPos);
#else
	light.dir = _WorldSpaceLightPos0.xyz;
#endif
	UNITY_LIGHT_ATTENUATION(attenuation, 0, i.worldPos);
	light.color = _LightColor0.rgb * attenuation;
	light.ndotl = DotClamped(i.normal, light.dir);
	return light;
}

UnityIndirect CreateIndirectLight(Interpolators i)
{
	UnityIndirect indirectLight;
	indirectLight.diffuse = 0;
	indirectLight.specular = 0;

#if defined(VERTEXLIGHT_ON)
	indirectLight.diffuse = i.vertexLightColor;
#endif

#if defined(FORWARD_BASE_PASS)
	indirectLight.diffuse += max(0, ShadeSH9(float4(i.normal,1))); 
#endif
	return indirectLight;
}
//---REFLECTION---///


void InitializeFragmentNormal(inout Interpolators i){
	// float2 du = float2(_HeightMap_TexelSize.x * 0.5, 0); 
	// float u1 = tex2D(_HeightMap, i.uv - du); 
	// float u2 = tex2D(_HeightMap, i.uv + du);
	// i.normal = float3(u1 - u2, 1, 0); 
	// // i.normal = float3(1, (h2 - h1) / delta.x, 0); 

	// float2 dv = float2(0, _HeightMap_TexelSize.y * 0.5); 
	// float v1 = tex2D(_HeightMap, i.uv - dv); 
	// float v2 = tex2D(_HeightMap, i.uv + dv); 
	// i.normal = float3(0, 1, v1 - v2); 
	i.normal = tex2D(_NormalMap, i.uv).xyz * 2 - 1;
	i.normal = i.normal.xyz; 
	// i.normal = normalize(i.normal); 
}
float4 MyFragmentProgram(Interpolators i) : SV_TARGET
{
	InitializeFragmentNormal(i); 
	float3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
	float3 albedo = tex2D(_MainTex, i.uv).rgb * _Tint.rgb;
	// albedo *= tex2D(_HeightMap, i.uv); 

	float3 specularTint;
	float oneMinusReflectivity;
	albedo = DiffuseAndSpecularFromMetallic(albedo, _Metallic, specularTint, oneMinusReflectivity);
	
	// float3 shColor = ShadeSH9(float4(i.normal,1)); 
	// return float4(shColor, 1); 

	return UNITY_BRDF_PBS(albedo, specularTint, oneMinusReflectivity, _Smoothness, i.normal, viewDir, CreateLight(i), CreateIndirectLight(i));

	// float3 lightColor = _LightColor0.rgb;
	// float3 lightDir = _WorldSpaceLightPos0.xyz;
	// float3 halfVector = normalize(lightDir + viewDir);
	// float3 diffuse = albedo * lightColor * DotClamped(lightDir, i.normal);
	// float3 specular = specularTint * lightColor * pow(DotClamped(halfVector, i.normal), _Smoothness * 100);
	// UnityLight light;
	// light.color = lightColor;
	// light.dir = lightDir;
	// light.ndotl = DotClamped(i.normal, lightDir);

	// UnityIndirect indirectLight;
	// indirectLight.diffuse = 0;
	// indirectLight.specular = 0;

}

#endif
//---REFLECTION---///
