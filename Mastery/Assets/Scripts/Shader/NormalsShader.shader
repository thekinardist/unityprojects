﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/NormalsShader"
{	
	Properties{
		_Tint ("Tint", Color) = (1,1,1,1)
		_MainTex("Texture", 2D) = "white" {}
	}
	SubShader{
		
		Pass{
			CGPROGRAM
			#pragma vertex MyVertexProgram
			#pragma fragment MyFragmentProgram

			#include "UnityCG.cginc"

			float4 _Tint;

		
			struct VertexData{
				float4 position : POSITION; 
				float3 normal : NORMAL; 
				float2 uv : TEXCOORD0; 
			};

				struct Interpolators{
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD0; 
				float3 normal : TEXCOORD1; 
			}; 

			Interpolators MyVertexProgram(VertexData v){
				Interpolators i; 
				i.position = UnityObjectToClipPos(v.position); 
				i.normal = v.normal; 
				i.uv = v.uv; 
				return i; 
			}
	

			float4 MyFragmentProgram (Interpolators i): SV_TARGET{
				return float4(i.normal * 0.5 + 0.5, 1) * _Tint; 
			}
		
			ENDCG 

		}
	}
}
