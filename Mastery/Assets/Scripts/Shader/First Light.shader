﻿// // Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// // Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// // Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// // Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// // Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader "Custom/First Light"
// {	
// 	Properties{
// 		_Tint ("Tint", Color) = (1,1,1,1)
// 		_MainTex("Albedo", 2D) = "white" {}
// 		// [NoScaleOffeset] _HeightMap("Heights", 2D) = "gray" {}
// 		[NoScaleOffeset] _NormalMap("Normals", 2D) = "bump" {}
// 		[Gamma] _Metallic ("Metallic", Range(0,1)) = 0 
// 		_Smoothness("Smoothness", Range(0,1)) = 0.1
// 	}
// 	SubShader{
		
// 		Pass{

// 			Tags{

// 				"LightMode" = "ForwardBase"
// 			}
// 			CGPROGRAM

// 			#pragma target 3.0
// 			#pragma multi_compile_VERTEXLIGHT_ON
// 			#pragma vertex MyVertexProgram
// 			#pragma fragment MyFragmentProgram

// 			#define FORWARD_BASE_PASS
// 			#include "My Lighting.cginc"

// 			// #include "UnityCG.cginc"
// 			// #include "UnityStandardBRDF.cginc"
// 			// #include "UnityStandardUtils.cginc"

// 			ENDCG 

// 		}
			
// 		Pass{

// 			Tags{

// 				"LightMode" = "ForwardAdd"
// 			}
// 			Blend One One
// 			ZWrite Off
// 			CGPROGRAM

// 			#pragma target 3.0
// 			#pragma multi_compile_fwdadd
// 			// #pragma multi_compile DIRECTIONAL_COOKIE POINT SPOT	

// 			#pragma vertex MyVertexProgram
// 			#pragma fragment MyFragmentProgram

// 			// #define POINT 

// 			#include "My Lighting.cginc"

// 			// #include "UnityCG.cginc"
// 			// #include "UnityStandardBRDF.cginc"
// 			// #include "UnityStandardUtils.cginc"

// 			ENDCG 

// 		}
// 	}
// }
