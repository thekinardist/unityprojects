﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : PersistableObject
{
    // public PersistableObject prefab; 
    public ShapeFactory shapeFactory;
    public KeyCode createKey = KeyCode.C;
    public KeyCode newGameKey = KeyCode.N;
    public KeyCode saveKey = KeyCode.S;
    public KeyCode loadKey = KeyCode.L;
    public KeyCode destroy = KeyCode.X;
    // Use this for initialization
    const int saveVersion = 2;
    public int levelCount; 
    int loadedLevelBuildIndex; 
    public PersistentStorage storage;
    public float CreationSpeed { get; set; }
    public float DestructionSpeed { get; set; }
    float creationProgress, destructionProgress;
    List<Shape> shapes;
    // string savePath; 
    void Start()
    {
        shapes = new List<Shape>();
        if (Application.isEditor){
            for(int i = 0; i < SceneManager.sceneCount; i++){
                Scene loadedScene = SceneManager.GetSceneAt(i); 
                if(loadedScene.name.Contains("Level ")){
                    SceneManager.SetActiveScene(loadedScene);
                    loadedLevelBuildIndex = loadedScene.buildIndex;  
                    return; 
                }
            }
            // Scene LoadedLevel = SceneManager.GetSceneByName("Level 1");
            // if (LoadedLevel.isLoaded)
            // {
            //     SceneManager.SetActiveScene(LoadedLevel);
            //     return;
            // }
        }
        StartCoroutine(LoadLevel(1));
        // savePath = Path.Combine(Application.persistentDataPath, "saveFile"); 

    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(createKey))
        {
            // CreateShape() ;
        }
        else if (Input.GetKeyDown(destroy))
        {
            // DestroyShape(); 
        }
        else if (Input.GetKeyDown(newGameKey))
        {
            BeginNewGame();
        }
        else if (Input.GetKeyDown(saveKey))
        {
            // Save(); 
            storage.Save(this, saveVersion);
        }
        else if (Input.GetKeyDown(loadKey))
        {
            // Load()
            BeginNewGame();
            storage.Load(this);
        }else {
            for(int i = 1; i <= levelCount; i++){
                //Can set these conditions to something else in order to start a new game(death for instance)
                if(Input.GetKeyDown(KeyCode.Alpha0 + i)){
                    BeginNewGame(); 
                    StartCoroutine(LoadLevel(i)); 
                    return; 
                }
            }
        }

        creationProgress += Time.deltaTime * CreationSpeed;
        while (creationProgress >= 1f)
        {
            creationProgress -= 1f;
            CreateShape();
        }
        destructionProgress += Time.deltaTime * DestructionSpeed;
        while (destructionProgress >= 1f)
        {
            destructionProgress -= 1f;
            DestroyShape();
        }
    }
    void BeginNewGame()
    {
        for (int i = 0; i < shapes.Count; i++)
        {
            Destroy(shapes[i].gameObject);
        }
        shapes.Clear();
    }

    void CreateShape()
    {
        // PersistableObject o = Instantiate(prefab); 
        Shape instance = shapeFactory.GetRandom();
        Transform t = instance.transform;
        t.localPosition = Random.insideUnitSphere * 2.3f;
        t.localRotation = Random.rotation;
        t.localScale *= Random.Range(0.1f, .8f);
        instance.SetColor(Random.ColorHSV(0f, 1f, 0.5f, 1f, 0.25f, 1f, 1f, 1f));
        shapes.Add(instance);
    }
    void DestroyShape()
    {
        if (shapes.Count > 0)
        {
            int index = Random.Range(0, shapes.Count);
            Destroy(shapes[index].gameObject);
            int lastIndex = shapes.Count - 1;
            shapes[index] = shapes[lastIndex];
            shapes.RemoveAt(lastIndex);
        }
    }

    IEnumerator LoadLevel(int levelBuildIndex)
    {
        //Show loading screen 
        enabled = false;
        if(loadedLevelBuildIndex > 0){
            yield return SceneManager.UnloadSceneAsync(loadedLevelBuildIndex); 
        }
        yield return SceneManager.LoadSceneAsync(levelBuildIndex, LoadSceneMode.Additive);
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(levelBuildIndex));
        loadedLevelBuildIndex = levelBuildIndex; 
        //Hide Loading Screen
        enabled = true;
    }
    public override void Save(GameDataWriter writer)
    {
        // writer.Write(-saveVersion); 
        writer.Write(shapes.Count);
        writer.Write(loadedLevelBuildIndex); 
        for (int i = 0; i < shapes.Count; i++)
        {
            writer.Write(shapes[i].ShapeId);
            writer.Write(shapes[i].MaterialId);
            shapes[i].Save(writer);
        }
    }

    public override void Load(GameDataReader reader)
    {
        int version = reader.Version;
        if (version > saveVersion)
        {
            Debug.LogError("Unsupported furture save version" + version);
            return;
        }
        int count = version <= 0 ? -version : reader.ReadInt();
        StartCoroutine(LoadLevel(version < 2 ? 1 : reader.ReadInt())); 
        for (int i = 0; i < count; i++)
        {
            // PersistableObject o = Instantiate(prefab); 
            int shapeId = version > 0 ? reader.ReadInt() : 0;
            int materialId = version > 0 ? reader.ReadInt() : 0;
            Shape instance = shapeFactory.Get(shapeId, materialId);
            instance.Load(reader);
            shapes.Add(instance);
        }
    }

    //--OLD SAVE AND LOAD STUFF--//
    // void Save(){
    // 	using(
    // 	var writer = 
    // 	new BinaryWriter(File.Open(savePath, FileMode.Create))){
    // 		writer.Write(objects.Count); 
    // 		for(int i = 0; i < objects.Count; i++){
    // 			Transform t = objects[i]; 
    // 			writer.Write(t.localPosition.x); 
    // 			writer.Write(t.localPosition.y); 
    // 			writer.Write(t.localPosition.z); 
    // 		}
    // 	}
    // }

    // void Load(){
    // 	BeginNewGame();

    // 	using(var reader = new BinaryReader(File.Open(savePath, FileMode.Open))){
    // 		int count = reader.ReadInt32(); 
    // 		for(int i = 0; i < count; i++){
    // 		Vector3 p; 
    // 		p.x = reader.ReadSingle(); 
    // 		p.y = reader.ReadSingle(); 
    // 		p.z = reader.ReadSingle(); 
    // 		Transform t = Instantiate(prefab); 
    // 		t.localPosition = p; 
    // 		objects.Add(t); 
    // 		}
    // 	}
    // }
    //--OLD SAVE AND LOAD STUFF--//
}
