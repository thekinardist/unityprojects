﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 

public class Fractal : MonoBehaviour {

public Mesh[] meshes; 
public Material material;
public int maxDepth, depth;  
public float childScale; 
private Material[] materials; 

private void InitializeMatrials(){
	materials = new Material[maxDepth + 1]; 
	for(int i = 0; i <= maxDepth; i++){
		materials[i] = new Material(material); 
		materials[i].color = Color.Lerp(Color.white, Color.yellow, (float)i/maxDepth); 
	}
}

private void Start(){
	if(materials == null){
		InitializeMatrials(); 
	}

	gameObject.AddComponent<MeshFilter>().mesh = meshes[Random.Range(0, meshes.Length)]; 
	gameObject.AddComponent<MeshRenderer>().material = materials[depth];
	// GetComponent<MeshRenderer>().material.color =; 
	if(depth < maxDepth){
		StartCoroutine(CreateChildren()); 
	}
}

	private static Vector3[] childDirections = {
	Vector3.down, 
	Vector3.right, 
	Vector3.left, 
	Vector3.forward, 
	Vector3.back, 
}; 

private static Quaternion[] childOrientations = {
	Quaternion.identity, 
	Quaternion.Euler(0f,0f,-90f), 
	Quaternion.Euler(0f,0f,90f), 
	Quaternion.Euler(90f, 0f,0f), 
	Quaternion.Euler(-90f,0f,0f), 
}; 
private void Initialize(Fractal parent,int childIndex){
	meshes = parent.meshes; 
	materials = parent.materials; 
	maxDepth = parent.maxDepth; 
	depth = parent.depth +1; 
	childScale = parent.childScale; 
	transform.parent = parent.transform;
	transform.localScale = Vector3.one * childScale; 
	transform.localPosition = childDirections[childIndex] * (0.5f + 0.5f * childScale); 
	transform.localRotation = childOrientations[childIndex]; 

}
	// Use this for initialization


	private IEnumerator CreateChildren(){
		for(int i = 0; i < childDirections.Length; i++){
			yield return new WaitForSecondsRealtime(Random.Range(0.1f,0.5f)); 
			new GameObject("Fractal Child").AddComponent<Fractal>().Initialize(this,i); 
		}
		// yield return new WaitForSecondsRealtime(0.5f); 
		// new GameObject("Fractal Child").AddComponent<Fractal>().Initialize(this, Vector3.up, Quaternion.identity);  
		// yield return new WaitForSecondsRealtime(0.5f); 
		// new GameObject("Fractal Child").AddComponent<Fractal>().Initialize(this, Vector3.right, Quaternion.Euler(0f,0f, -90f)); 
		// yield return new WaitForSecondsRealtime(0.5f); 
		// new GameObject("Fractal Child").AddComponent<Fractal>().Initialize(this, Vector3.left, Quaternion.Euler(0f,0f,90f)); 
	}
	// Update is called once per frame
	void Update () {
			Component[] bodies; 
	bodies = GetComponentsInChildren(typeof(Transform)); 
		foreach(Transform body in bodies){
			 body.Rotate(0,Mathf.Sin(40 * Time.deltaTime),0); 
		}
	}
}
