using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Basics : MonoBehaviour
{
	public Transform cubeA, cubeB,cubeC,cubeD;
	

	void Start()
	{	
		DOTween.Init(false, true, LogBehaviour.ErrorsOnly);

		//transform.DOMoveX(45, 1).SetDelay(2).SetEase(Ease.OutQuad).OnComplete(MyCallback);

		 Sequence mySequence = DOTween.Sequence();
		mySequence.Append(cubeA.DOMoveZ(20, 2).SetEase(Ease.InOutSine));
		mySequence.Join(cubeA.DOScale(10f,2).SetEase(Ease.InOutSine));

		mySequence.Join(cubeB.DOMoveX(20, 2).SetEase(Ease.InOutSine));
		mySequence.Join(cubeB.DOScale(10f,2).SetEase(Ease.InOutSine));  

		mySequence.Join(cubeC.DOMoveZ(20, 2).SetEase(Ease.InOutSine));
		mySequence.Join(cubeC.DOScale(10f,2).SetEase(Ease.InOutSine)); 
		
		mySequence.Join(cubeD.DOMoveX(20, 2).SetEase(Ease.InOutSine));
		mySequence.Join(cubeD.DOScale(10f,2).SetEase(Ease.InOutSine)); 
		mySequence.SetLoops(-1,LoopType.Yoyo); 




		
		}

	void FixedUpdate(){
		
	}
}