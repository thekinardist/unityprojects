		#include "UnityStandardBRDF.cginc"
		#include "UnityPBSLighting.cginc"


		#if !defined(MY_LIGHTING_INCLUDED)
		#define MY_LIGHTING_INCLUDED


		float4 _Tint; 
		sampler2D _MainTex; 

		struct Interpolators{
		float4 position: SV_POSITION; 
		float3 normal: NORMAL; 
		float2 uv: TEXCOORD0; 
		};

		struct VertexData{
		float4 position: POSITION; 
		float3 normal: NORMAL; 
		float2 uv: TEXCOORD0; 
		}; 
		#endif

		Interpolators MyVertexProgram(VertexData v) {
			Interpolators i; 
			i.position = UnityObjectToClipPos(v.position);
			i. normal = mul(transpose((float3x3)unity_ObjectToWorld), v.normal); 
			i.normal = normalize(i.normal);  
			return i; 
		}

		float4 MyFragmentProgram(Interpolators i): SV_TARGET {
		
		i.normal = normalize(i.normal);
		float3 lightDir = _WorldSpaceLightPos0.xyz;
		float3 lightColor = _LightColor0.rgb;
		float3 albedo = tex2D(_MainTex, i.uv).rgb * _Tint.rgb; 
		float3 diffuse = albedo * lightColor * DotClamped(lightDir, i.normal); 
		return float4(diffuse, 1) * _Tint; 		

		}

