﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/TheStart" {
	
	Properties{
	_Tint("Tint", Color) = (1,1,1,1)
	_MainTex("Texture", 2D) = "white" {}
	_WaveScale("Wave scale", Range(0.02,0.15)) = 0.5
	_ReflDistort("Reflection distort", Range(0,1.5)) = 0.5
	_RefrDistort("Refraction distort", Range(0,1.5)) = 0.4
	_RefrColor("Refraction color", Color) = (0.34,0.85,0.92,1)
	_ReflectionTex ("Environment Reflection",2D) = "" {}
	_RefractionTex ("Environment Refraction",2D) = "" {}
	_Fresnel ("Fresnel (A)",2D) = ""{}
	_BumpMap ("Bumpmap (RBG) ", 2D) = "" {}

	}
	SubShader{


		Pass{
		CGPROGRAM

		#pragma vertex MyVertexProgram
		#pragma fragment MyFragmentProgram
		#include "UnityCG.cginc"

		float4 _Tint; 
		float _WaveScale; 
		float _ReflDistort; 
		float _RefrDistort;
		float4 _RefrColor; 
		float _ReflectionTex; 
		float _RefractionTex; 
		float _Fresnel;
		float _BumpMap;  
		//texture
		sampler2D _MainTex; 

		struct Interpolators{
		float4 position: SV_POSITION; 
		float2 uv: TEXCOORD0; 
		};

		struct VertexData{
		float4 position: POSITION; 
		float2 uv: TEXCOORD0; 
		}; 

		Interpolators MyVertexProgram(VertexData v) {
		//return position; 
		Interpolators i; 
		i.position = UnityObjectToClipPos (v.position); 
		i.uv = v.uv;
		return i; 
		}

		float4 MyFragmentProgram(Interpolators i): SV_TARGET {
		//return _Tint;
		//return float4(i.localPosition += 0.5, 1) * _Tint; 
		 return float4(i.uv,1,1) * _Tint; 
		 //return tex2D(_MainTex, i.uv)* _Tint; 
//		return _WaveScale;
//		return _ReflDistort; 
//		return _RefrDistort; 
//		return _RefrColor; 
//		return _ReflectionTex; 
//		return _RefractionTex; 
//		return _Fresnel; 
//		return _BumpMap; 

		}

		ENDCG

		}
	}
}
