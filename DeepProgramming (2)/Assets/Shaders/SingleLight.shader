
Shader "Custom/SingleLight" {
	
	Properties{
	_Tint("Tint", Color) = (1,1,1,1)
	_MainTex("Albedo", 2D) = "white" {}
	//_MainTex("Texture", 2D) = "white" {}
	_WaveScale("Wave scale", Range(0.02,0.15)) = 0.5
	_ReflDistort("Reflection distort", Range(0,1.5)) = 0.5
	_RefrDistort("Refraction distort", Range(0,1.5)) = 0.4
	_RefrColor("Refraction color", Color) = (0.34,0.85,0.92,1)
	_ReflectionTex ("Environment Reflection",2D) = "" {}
	_RefractionTex ("Environment Refraction",2D) = "" {}
	_Fresnel ("Fresnel (A)",2D) = ""{}
	_BumpMap ("Bumpmap (RBG) ", 2D) = "" {}

	}
	SubShader{


		Pass{

			Tags{
				"LightMode" = "ForwardBase"
			}
		CGPROGRAM
		#pragma target 3.0
		#pragma vertex MyVertexProgram
		#pragma fragment MyFragmentProgram
		#include "MultiLightingBase.cginc"

		ENDCG

		}
				Pass{

			Tags{
				"LightMode" = "ForwardAdd"
			}

		Blend One One
		ZWrite Off 
		CGPROGRAM
		#pragma target 3.0
		#pragma vertex MyVertexProgram
		#pragma fragment MyFragmentProgram
		#include "MultiLightingBase.cginc"

		ENDCG

		}
	}
}
