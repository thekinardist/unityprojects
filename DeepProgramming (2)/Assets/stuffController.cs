﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class stuffController : MonoBehaviour {
private Score sh;
private makeNewStuff mk; 

	// Use this for initialization
	void Start () {
		sh = GameObject.Find("ScoreHolder").GetComponent<Score>();
		mk = GameObject.Find("StuffMaker").GetComponent<makeNewStuff>(); 

		DOTween.Init(false, true, LogBehaviour.ErrorsOnly);

		Sequence stuffMove = DOTween.Sequence();
		stuffMove.Append(this.transform.DOMoveY(.5f,1).SetEase(Ease.InOutElastic));
		stuffMove.Join(this.transform.DORotate(new Vector3(0, 0, 15), 1).SetEase(Ease.InOutElastic));  
		stuffMove.Join(this.transform.DOMoveY(.5f,1).SetEase(Ease.InOutElastic)); 
		stuffMove.Join(this.transform.DORotate(new Vector3(0, 0, 15), 1).SetEase(Ease.InOutElastic));  
		stuffMove.Join(this.transform.DOMoveY(.5f,1).SetEase(Ease.InOutElastic)); 
		stuffMove.Join(this.transform.DORotate(new Vector3(0, 0, 15), 1).SetEase(Ease.InOutElastic));  
		stuffMove.SetLoops(-1,LoopType.Yoyo); 
	}
	

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Player")){
			if(sh.money >= 5 && mk.stuffGot == false){
			sh.plusStuff();
			sh.minusMoney(); 
			mk.stuffGot = true; 
			Destroy(gameObject); 
			}
		}
	}

		// Update is called once per frame
	void Update () {

	}

}
