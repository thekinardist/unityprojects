﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : MonoBehaviour {
	private Renderer rend; 
	private Color altColor; 

	// Use this for initialization
	void Start () {
	rend = GetComponent<Renderer>(); 
	rend.material.color = altColor; 

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate(){
		float r = this.transform.localPosition.x; 
		float g = this.transform.position.y; 
		float b = this.transform.position.z; 

		altColor.r = r % 256; 
		altColor.g = g % 256; 
		altColor.b = b % 256; 

		rend.material.color = altColor; 

	}
}
