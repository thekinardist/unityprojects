﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScaleTransformation : Transformation {

	public  Vector3 scale;
	private float counter; 
	public override Vector3 Apply(Vector3 point){
		point.x *= scale.x; 
		point.y *= scale.y; 
		point.z *= scale.z; 
		return point; 
	}

	void Start(){

	}

	void FixedUpdate(){
		counter++;
		if(counter <= 220){
			scale.x = 1; 
			scale.y = 1; 
			scale.z = 1; 
		}else if(counter > 220 && counter <= 420){
			scale.x = 2; 
			scale.y = 2; 
			scale.z = 2; 
		}else if(counter > 520 && counter <= 720){
			scale.x = 3; 
			scale.y = 3; 
			scale.z = 3; 
		}else if(counter > 920 && counter <= 1120){
			scale.x = 4; 
			scale.y = 4; 
			scale.z = 4; 
		} else if(counter > 1320 && counter <= 1520){
			scale.x = 5; 
			scale.y = 5; 
			scale.z = 5; 
		}
		if(counter > 1820){
			counter = 0; 
		}
	}
}
