using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class GridsNoCoroutine : MonoBehaviour {

	// Use this for initialization
	public int xSiz, ySiz; 
	private Vector3[] vertices; 
	private Mesh mesh; 
	private float move = 30.0f; 

private void Awake(){
		Generate();
		//StartCoroutine(Generate());  
}

private void FixedUpdate(){
	move += Time.deltaTime;
//this.transform.Rotate(0, 0, Mathf.Cos(move)); 
}
private void Generate(){
		GetComponent<MeshFilter>().mesh = mesh = new Mesh(); 
		mesh.name = "Procedural Grid"; 

		vertices = new Vector3[(xSiz + 1) * (ySiz + 1)]; 
		Vector2[] uv = new Vector2[vertices.Length]; 
		Vector4[] tangents = new Vector4[vertices.Length]; 
		Vector4 tangent = new Vector4(1f,0f,0f,-1f);
			for(int i = 0, y = 0; y <= ySiz; y++){
			for(int x = 0; x <= xSiz; x++, i++){
				vertices[i] = new Vector3(x,y);
				uv[i] = new Vector2((float)x / xSiz, (float) y / ySiz); 
				tangents[i] = tangent; 
				}
			}
		mesh.vertices = vertices;
		mesh.uv = uv; 
		mesh.tangents = tangents; 

						int[] triangles = new int[xSiz * ySiz * 6];
						for (int ti = 0, vi = 0, y1 = 0; y1 < ySiz; y1++, vi++){
							for (int x1 = 0; x1 < xSiz; x1++, ti += 6, vi++){
							triangles[ti] = vi; 
							triangles[ti + 3] = triangles[ti + 2] = vi + 1;
							triangles[ti + 4] = triangles[ti + 1] = vi + xSiz + 1;
							triangles[ti + 5] = vi + xSiz + 2; 
			}
		}	
		mesh.triangles = triangles;
		mesh.RecalculateNormals();  
	
	}


}