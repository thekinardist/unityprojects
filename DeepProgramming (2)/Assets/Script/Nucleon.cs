﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody))]

public class Nucleon : MonoBehaviour {
	private Color altColor; 
	public float attractionForce; 
	public AudioClip sfx; 


	private AudioSource contact { get { return GetComponent<AudioSource>(); } }

	Rigidbody body; 

	// Use this for initialization
	void Awake(){
	body = GetComponent<Rigidbody>(); 
	}
	void Start () {
	GetComponent<MeshRenderer> ().material.color = altColor;  

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision other){
		contact.PlayOneShot(sfx); 
	}

	private void FixedUpdate(){

		body.AddForce(transform.localPosition * -attractionForce); 
		float cr = this.transform.position.x; 
	 	//float cb = this.transform.position.y; 
	 	float cg = this.transform.position.z; 

	 	altColor.r = cr %256; 
	 	altColor.b = 20;
	 	altColor.g = cg %256; 
		GetComponent<MeshRenderer> ().material.color = altColor;  
	}
}
