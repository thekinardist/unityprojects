﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTranslation : Transformation {

public Vector3 rotation; 
private float counter; 
public Transform thing; 

	private void Start(){

	}
	public override Vector3 Apply (Vector3 point){
		float radX = rotation.x * Mathf.Deg2Rad; 
		float radY = rotation.y * Mathf.Deg2Rad; 
		float radZ = rotation.z * Mathf.Deg2Rad; 

		float sinX = Mathf.Sin(radX); 
		float cosX = Mathf.Cos(radX); 
		float sinY = Mathf.Sin(radY); 
		float cosY = Mathf.Cos(radY); 
		float sinZ = Mathf.Sin(radZ); 
		float cosZ = Mathf.Cos(radZ); 

		Vector3 xAxis = new Vector3(
			cosY * cosZ, 
			cosX * sinZ + sinX * sinY * cosZ, 
			sinX * sinZ - cosX * sinY * cosZ
			); 

		Vector3 yAxis = new Vector3(
			-cosY * sinZ, 
			cosX * cosZ - sinX * sinY * sinZ, 
			sinX * cosZ + cosX * sinY * sinZ
			); 
		Vector3 zAxis = new Vector3(
			sinY, 
			-sinX * cosY, 
			cosX * cosY
			); 

		return xAxis * point.x + yAxis * point.y + zAxis * point.z;
	}

	void FixedUpdate(){
		float move = 30.0f; 
		float adder = 1.0f; 
		//float counter = 0f; 
		counter += adder; 
		
		//thing.DORotate(new Vector3(0, 135,0),1f).SetLoops(-1,Yoyo); 
		move += Time.deltaTime;
		if(counter <= 200){
		rotation.y += move * Time.deltaTime; 
		}else if(counter > 200 && counter <= 400){
			rotation.x += move * Time.deltaTime; 
		} else if(counter > 400 && counter <= 600){
			rotation.z += move * Time.deltaTime; 
		}

		if(counter >= 600){
			counter = 0; 
		}
	}
}
