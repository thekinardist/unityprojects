﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class Score : MonoBehaviour {
	public Text moneyDisplay;
	public Text stuffDisplay; 
	public Text goBuy; 
	public Text gotStuff; 

	public AudioClip[] sfx; 
	private AudioSource playSound {get {return GetComponent<AudioSource>(); }}
	public int money;
	public int stuff; 
	// Use this for initialization
	void Start () {
		displayText(); 
		gameObject.AddComponent<AudioSource>(); 
		goBuy.enabled = false; 
		gotStuff.enabled = false; 
	}
	
	public void displayText() {
		moneyDisplay.text = "Money: " + money;
		stuffDisplay.text = "Stuff: " + stuff; 
	}
	// Update is called once per frame
	void Update () {

		if(money == 5){
			StartCoroutine("buyOn"); 
		}
		if(stuff == 1){
			StartCoroutine("stuffOn"); 
		}
	}

	public void plusMoney(){
		money++; 
		playSound.PlayOneShot(sfx[0]); 
		displayText(); 
	}
	public void minusMoney(){
		money -= 5; 
		displayText(); 
	}

	public void plusStuff(){
		stuff++;
		playSound.PlayOneShot(sfx[1]); 
		displayText(); 
 	}

 	IEnumerator buyOn(){
 		goBuy.enabled = true; 	
 		yield return new WaitForSeconds(3.0f); 
 		goBuy.enabled = false; 
 	}

 	IEnumerator stuffOn(){
 		gotStuff.enabled = true;
 		yield return new WaitForSeconds(3.0f); 
 		gotStuff.enabled = false;
 
 	}
}
