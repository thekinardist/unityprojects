
using UnityEngine; 
using System.Collections; 


public class Node{
public int x; 
public int y; 
public bool walk; 

public Vector3 pos; 


public int g ; 
public int h; 

public Node pre; 

public Node(bool mWalk, int mX, int mY, Vector3 mPos)
	{
	x = mX; 
	y = mY; 
	walk = mWalk; 
	pos = mPos; 
	}
}