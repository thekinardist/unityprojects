﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridControllerLesson : MonoBehaviour {

    Node[,] grid;

    public float totalX;
    public float totalY;
    public float nodeSize;
    private int gridNumX;
    private int gridNumY;
    
    public LayerMask unwalkMask;
    
    // Use this for initialization
    void Start()
    {
        
        gridNumX = Mathf.RoundToInt(totalX / nodeSize);
        gridNumY = Mathf.RoundToInt(totalY / nodeSize);
        CreateGrid();
    }

    void CreateGrid()
    {
        grid = new Node[gridNumX, gridNumY];

        Vector3 lowerLeft = transform.position - Vector3.right * totalX / 2 - Vector3.forward * totalY / 2;

        for (int x = 0; x < gridNumX; x++)
        {
            for (int y = 0; y < gridNumY; y++)
            {
                Vector3 nodeCenter = lowerLeft + Vector3.right * (x * nodeSize * 1.5f) + Vector3.forward * (y * nodeSize * 1.5f);
                bool walk = !(Physics.CheckSphere(nodeCenter, nodeSize/2f, unwalkMask));
                grid[x, y] = new Node(walk, x, y, nodeCenter);
            }
        }
    }

    public List<Node> GetNeighbors(Node node)
    {
        List<Node> neighbors = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                int checkX = node.x + x;
                int checkY = node.y + y;

                if (checkX >= 0 && checkX < gridNumX && checkY >= 0 && checkY < gridNumY)
                {
                    neighbors.Add(grid[checkX, checkY]);
                }
            }
        }

        return neighbors;
    }


    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        float percentX = (worldPosition.x + totalX / 2) / totalX;
        float percentY = (worldPosition.z + totalY / 2) / totalY;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((gridNumX - 1) * percentX);
        int y = Mathf.RoundToInt((gridNumY - 1) * percentY);
        return grid[x, y];
    }

    public List<Node> path;
    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(totalX, 1, totalY));

        if (grid != null)
        {
            foreach (Node n in grid)
            {
                Gizmos.color = (n.walk) ? Color.white : Color.red;
                if (path != null)
                    if (path.Contains(n))
                        Gizmos.color = Color.black;
                Gizmos.DrawCube(n.pos, Vector3.one * (nodeSize - 0.1f));
            }
        }
    }
}