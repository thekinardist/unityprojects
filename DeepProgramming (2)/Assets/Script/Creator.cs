﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs; 

public class Creator : MonoBehaviour {
	public int numberOfSpawners; 
	public int increase; 
	public float radius, tiltAngle; 
	public ThingSpawner spawnerPrefab; 


	void Awake(){
		for(int i = 0; i < numberOfSpawners; i++){
		createSpawner(i);
		}
		// StartCoroutine("AddOn"); 
	}

	void Update(){
	
	}

		IEnumerator AddOn(){
		yield return new WaitForSeconds(2f); 
		numberOfSpawners += increase; 
		radius += increase; 
		StartCoroutine("AddOn"); 
	}

	void createSpawner(int index){
		Transform rotater = new GameObject("Rotater").transform; 
		rotater.SetParent(transform, false); 
		rotater.localRotation = Quaternion.Euler(0f, index * 360.0f/numberOfSpawners, 0f); 
		ThingSpawner spawner = Instantiate<ThingSpawner>(spawnerPrefab); 
		spawner.transform.SetParent(rotater, false); 
		spawner.transform.localPosition = new Vector3(0f,0f,radius); 
		spawner.transform.localRotation = Quaternion.Euler(tiltAngle,0f,0f); 
	}

	void FixedUpdate(){
	this.transform.Rotate(0,-30 *Time.deltaTime,0); 
	}
}
