﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {
	private int  currentLevel = 1; 

	public List<GameObject> LvlDataObj = new List<GameObject>(); 
	//ENEMIES//
	public GameObject [] easy; 
	public GameObject[] medium; 
	public GameObject[] hard; 
	public GameObject[] EnemyArrayList; 
	//ENEMIES//


	public List<GameObject[]> all = new List<GameObject[]>(); 

	// Use this for initialization
	void Start () {

	for(int i = 0; i < LvlDataObj[currentLevel - 1].GetComponent<LevelDataScript>().enemySpawn.Count; i++){
		int tempLvlNum = LvlDataObj[currentLevel -1].GetComponent<LevelDataScript>().enemySpawn[i]; 
		if(i <= 1){
		popList(easy); 
		}else if(i == 2){
			popList(medium); 
		}
	}
	all = new List<GameObject[]>();

	popList(easy); 
	popList(hard); 
	popList(medium); 
	// all.Add(easy); 

	Debug.Log(easy); 
	int temp = Random.Range(1,easy.Length); 
	GameObject tempOb; 
	tempOb = Instantiate(easy[temp -1]);
	tempOb.transform.position = new Vector2(0,0); 
	tempOb.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform,false); 
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void popList(GameObject[] array){
	for(int i = 0; i < array.Length; i++){
		all.Add(array); 
		} 
	}
}
