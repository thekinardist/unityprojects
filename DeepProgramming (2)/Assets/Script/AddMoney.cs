﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddMoney : MonoBehaviour {
	private Score sh;

	// Use this for initialization
	void Start () {
	sh = GameObject.Find("ScoreHolder").GetComponent<Score>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}	

	void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Player")) {
	sh.plusMoney();
	Destroy(gameObject); 
		
	}
}
}
