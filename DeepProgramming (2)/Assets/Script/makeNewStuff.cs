﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class makeNewStuff : MonoBehaviour {
	public bool stuffGot; 
	public GameObject stuff; 
	// Use this for initialization
	void Start () {
		stuffGot = false; 
	}


	
	// Update is called once per frame
	void Update () {
	}

	public void OnTriggerExit(Collider other){
		if(other.CompareTag("Player") && stuffGot == true){
			Instantiate(stuff, this.transform.position, Quaternion.identity);
			stuffGot = false; 
		}
	}
}
