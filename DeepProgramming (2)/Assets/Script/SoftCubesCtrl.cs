﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoftCubesCtrl : MonoBehaviour {
	GameObject cusCube; 
	public float multiplier; 
	public int adder; 
	public int adder2; 
	public int adder3; 
	
	// Use this for initialization
	void Start () {
	cusCube = GameObject.Find("CustomCube"); 
	CustomCube cusScript = cusCube.GetComponent<CustomCube>(); 
	
	for(int i = 0; i < multiplier; i++){
		Vector3 position = new Vector3(Random.Range(-50,50), Random.Range(-50,50), Random.Range(-50,50)); 
		Instantiate(cusCube, position, Quaternion.identity); 
		cusScript.xSize += adder;
		cusScript.ySize += adder2; 
		cusScript.zSize += adder3; 
		}
	}
	
	// Update is called once per frame
	void Update () {

	}
	void FixedUpdate(){
		this.transform.Rotate(0, 30* Time.deltaTime,0);
 	}
}
