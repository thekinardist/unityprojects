﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformerInput : MonoBehaviour {

	public float force = 10f; 
	// Use this for initialization
	void Start () {
	 	
	}
	
	// Update is called once per frame
	void Update () {
			if(Input.GetMouseButton(0)){
			HandleInput(); 
		}
		
	}

	public float forceOffset = 0.1f; 
	void HandleInput(){
		Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition); 
		RaycastHit hit; 

		if(Physics.Raycast(inputRay, out hit)){
			MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>(); 

			if(deformer){
				Vector3 point = hit.point;
				point += hit.normal * forceOffset;  
				deformer.AddDeformingForce(point, force); 
				Debug.Log("Works");
			}
		}
	}

	// public void AddDeformingForce(Vector3 point, float force){
	// 	for(int i = 0; i < displacedVertices.Length; i++){
	// 		AddForceToVertex(i, point, force); 
	// 	}
	// }

	// void AddForceToVertex(int i, Vector3 point, float force){

	// }
}
