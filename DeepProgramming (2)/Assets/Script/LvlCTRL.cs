﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlCTRL : MonoBehaviour {
	GameObject ringSpawner;
	public float multiplier;
	public int ohterMultiplier; 
	public GameObject cam; 
	private float rsPos; 
	private bool canGo = false; 

	// Use this for initialization
	void Awake(){
	ringSpawner = GameObject.Find("SphereSpawner"); 
	rsPos = ringSpawner.transform.localPosition.y;
	StartCoroutine("Go"); 

	}
	void Start () {
		SpawnerRing spawnScript = ringSpawner.GetComponent<SpawnerRing>(); 
		for(int i = 0; i < multiplier; i++){
			Instantiate(ringSpawner, new Vector3(0,rsPos += multiplier,0), Quaternion.identity);
				spawnScript.radius = spawnScript.radius + multiplier;
				//spawnScript.radius = multiplier * 2; 
				spawnScript.numberOfSpawners = ohterMultiplier; 
	}

}
	IEnumerator Go(){
		yield return new WaitForSeconds(10.0f); 
		canGo = true; 

	}
	void FixedUpdate(){
		if(canGo){
		cam.transform.Translate(Vector3.forward * (10* Time.deltaTime));	

		}
	}
}
