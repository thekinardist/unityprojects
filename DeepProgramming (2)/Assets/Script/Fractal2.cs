using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fractal2 : MonoBehaviour
{


	//keeping track of invoke//
	private float counter = 0;
	public bool startWave = false; 
	private float Move;
	private float Move1; 
	private int depth;
	private Color altColor;
	//private Renderer renderer; 


	//static float t = 0.0f; 
	 
	public Mesh mesh;
	

	public Material material;
	//private GameObject sloan;
	public int maxDepth;
	public float childScale;
	public float divider;

	// Use this for initialization
	private void Start ()
	{
		gameObject.AddComponent<MeshFilter> ().mesh = mesh; 
		gameObject.AddComponent<MeshRenderer> ().material = material; 
		//GetComponent<Renderer>().material.mainTexture = movTexture; 
		//GetComponentsInParent<Renderer> ().material.mainTexture = movTexture; 
		//GetComponent<MeshRenderer> ().material.color = Color.Lerp (Color.black, Color.yellow, (float)depth / maxDepth);
		GetComponent<MeshRenderer> ().material.color = altColor;  
		//GetComponent<MeshRenderer>().material.mainTexture = movTexture; 
		if (depth < maxDepth) {
			StartCoroutine (CreateChildren ()); 
		}

		StartCoroutine (WaveStart ()); 
	}

	private static Vector3[] childDirections = {
		Vector3.up, Vector3.right, Vector3.left, Vector3.forward, Vector3.back, 
	};

	private static Quaternion[] childOrientations = {
		Quaternion.identity, 
		Quaternion.Euler (0f, 0f, -90f), 
		Quaternion.Euler (0f, 0f, 90f), 
		Quaternion.Euler (90f, 0f, 0f), 
		Quaternion.Euler (-90f, 0f, 0f),
	};

	private IEnumerator CreateChildren ()
	{
		for (int i = 0; i < childDirections.Length; i++) {
			yield return new WaitForSeconds (0.5f); 
			new GameObject ("Fractal Child").AddComponent<Fractal2> ().Initialize (this, i); 
			yield return new WaitForSeconds (0.5f); 
			counter++; 
		}
	}

	private IEnumerator WaveStart(){
		yield return new WaitForSeconds (15.0f); 
		startWave = true; 
	}

	private void Initialize (Fractal2 parent, int childIndex)
	{	
		mesh = parent.mesh; 
		material = parent.material; 
		maxDepth = parent.maxDepth; 
		depth = parent.depth + 1; 
		childScale = parent.childScale; 
		transform.parent = parent.transform; 
		transform.localScale = Vector3.one * childScale; 
		transform.localPosition = childDirections [childIndex] * (0.5f + 0.5f * childScale); 
		transform.localRotation = childOrientations [childIndex];

		//transform.localRotation = orientation; 
		//transform.localPosition = direction * (0.5f + 0.5f * childScale); 

	}

	private void FixedUpdate ()
	{
		transform.Rotate (0, 30 * Time.deltaTime, 0); 
		float cr = transform.localPosition.x; 
		float cg = transform.localPosition.y; 

		altColor.r = cr % 256;
		altColor.b = 40; 
		altColor.g = cg % 256; 
		//GetComponent<MeshRenderer> ().material.color = altColor; 

		if (startWave == true) {
			Vector3 newPosition = transform.localPosition; 
			newPosition.y += Time.deltaTime;
			transform.localPosition = newPosition; 
//			Move1 += 5 * Time.deltaTime; 
//			Vector3 newPosition = transform.localPosition; 
//			newPosition.y += 5; 
//			transform.localPosition = newPosition;  
//
//			float minimum = -1.0f; 
//			float maximum = 1.0f; 
//			float lerped = Mathf.Lerp (minimum, maximum + 3, t); 
//
//			t  = 0.5f * Time.deltaTime;
//
//			transform.localPosition = new Vector3 (lerped, lerped, lerped);
//
//			if (t > 1.0f) {
//				float temp = maximum; 
//				maximum = minimum; 
//				minimum = temp; 
//				t = 0.0f; 
//			}
		}
	}

	void Update ()
	{
		if (counter >= 6) {
			StopCoroutine (CreateChildren ()); 
		}	
	}
//
//
//	private IEnumerator frack(){
//		counter++; 
//		yield return new WaitForSecondsRealtime (3.0f); 
//		new GameObject ("Fractal Child").AddComponent<Fractal> ().Initialize (this, Vector3.up, Quaternion.identity); 
//		yield return new WaitForSecondsRealtime (3.0f); 
//		new GameObject ("Fractal Child").AddComponent<Fractal> ().Initialize (this, Vector3.right,Quaternion.Euler(0, 0,-90.0f));
//		yield return new WaitForSecondsRealtime (3.0f); 
//		new GameObject ("Fractal Child").AddComponent<Fractal> ().Initialize (this, Vector3.left,Quaternion.Euler(0,0,90.0f));
//		yield return new WaitForSecondsRealtime (3.0f); 
//		new GameObject ("Fractal Child").AddComponent<Fractal> ().Initialize (this, Vector3.down,Quaternion.Euler(0,0,180));
//	}
}
