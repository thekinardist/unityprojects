﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
//*****FOR IRREGULAR SPAWNING*****//
public struct FloatRange{
	public float min, max; 
	public float RandomInRange{
		get {
			return Random.Range(min,max); 
		}
	}
}
//*****FOR IRREGULAR SPAWNING*****//

public class ThingSpawner : MonoBehaviour {

	//*****FOR IRREGULAR SPAWNING*****//

	//*****FOR IRREGULAR SPAWNING*****//

	//public float timeBetweenSpawns;
	float timeSinceLastSpawn;

	public FloatRange timeBetweenSpawns, scale, randomVelocity; 
 
 	float currentSpawnDelay; 

	public Things[] thingPrefabs;



	void FixedUpdate(){
		this.transform.Rotate(0,30 * Time.deltaTime,0); 
		timeSinceLastSpawn += Time.deltaTime; 
		if(timeSinceLastSpawn >= currentSpawnDelay){
			timeSinceLastSpawn -= currentSpawnDelay; 
			currentSpawnDelay = timeBetweenSpawns.RandomInRange;
			spawnThings(); 	
		}	
	}
	public float vel;
 
	void spawnThings(){
		Things prefab = thingPrefabs[Random.Range(0,thingPrefabs.Length)]; 
		Things spawn = Instantiate<Things>(prefab); 

		spawn.transform.localPosition = transform.position; 

		spawn.transform.localScale = Vector3.one * scale.RandomInRange; 
		spawn.transform.localRotation = Random.rotation; 

		// spawn.Body.velocity = transform.up * vel + Random.onUnitSphere * randomVelocity.RandomInRange;
		//spawn.Body.angularVelocity = Random.onUnitSphere * angularVelocity.RandomInRange;	
	}
}
